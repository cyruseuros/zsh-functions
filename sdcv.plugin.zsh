def () {sdcv -c --only-data-dir \
             --data-dir=$STARDICT_DATA_DIR/Dict $* | less -R}

syn () {sdcv -c --only-data-dir \
             --data-dir=$STARDICT_DATA_DIR/Thesaurus $* | less -R}

dec () {sdcv -c --only-data-dir \ # decrypt
             --data-dir=$STARDICT_DATA_DIR/Specialty $* | less -R}
