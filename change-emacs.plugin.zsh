ce () {
    local -a EMACS_DIR EMACS_REPO EMACS_BRANCH
    case $1 in
        spacemacs)
            EMACS_DIR="${PROJECT_DIR}/spacemacs"
            EMACS_REPO='https://github.com/syl20bnr/spacemacs.git'
            EMACS_BRANCH=develop
            EMACS_CONFIG_REPO='https://gitlab.com/jjzmajic/spacemacs-config.git'
            EMACS_CONFIG_DIR=~/.spacemacs.d
            ;;
        doom-emacs)
            EMACS_DIR="${PROJECT_DIR}/doom-emacs"
            EMACS_REPO='https://github.com/hlissner/doom-emacs.git'
            EMACS_BRANCH=develop
            EMACS_CONFIG_REPO='https://gitlab.com/jjzmajic/doom-config.git'
            EMACS_CONFIG_DIR=~/.doom.d
            ;;
        centaur-emacs)
            EMACS_DIR="${PROJECT_DIR}/centaur-emacs"
            EMACS_REPO='https://github.com/seagle0128/.emacs.d.git'
            EMACS_BRANCH=master
            ;;
        straightmacs)
            EMACS_DIR="${PROJECT_DIR}/straightmacs"
            EMACS_REPO='https://gitlab.com/jjzmajic/straightmacs.git'
            EMACS_BRANCH=master
            ;;
        my:macs)
            EMACS_DIR="${PROJECT_DIR}/my:macs"
            EMACS_REPO='https://gitlab.com/jjzmajic/mymacs.git'
            EMACS_BRANCH=master
            ;;
        *) echo 'No such Emacs!';;
    esac

    if [[ -n "$EMACS_DIR" ]]; then
        if [[ ! -d ~/projects ]]; then
            mkdir ~/projects
        fi
        if [[ ! -d $EMACS_DIR ]]; then
            git clone $EMACS_REPO $EMACS_DIR
            pushd $EMACS_DIR
            git checkout $EMACS_BRANCH
            popd
        fi
        ln -sfn $EMACS_DIR ~/.emacs.d
    fi

    if [[ -n "$EMACS_CONFIG_DIR" ]]; then
        if [[ ! -d $EMACS_CONFIG_DIR ]]; then
            git clone $EMACS_CONFIG_REPO $EMACS_CONFIG_DIR
        fi
    fi
}

# Local Variables:
# mode: shell-script
# End:
